var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app     = express();
const Vote = require('./models/voteModel');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function(){
});

app.use(cors())
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

app.get('/vote', (req, res) => {
    Vote.find({},(err, voteList) =>{
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(voteList)
    });
    //Item.find({name: 'Quijote'}, callback);
});

app.post('/vote', (req, res) => {
    const vote = new Vote(req.body);
    vote.save((err, storedVote) =>{
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(storedVote)
    });
});

app.listen(3000);
